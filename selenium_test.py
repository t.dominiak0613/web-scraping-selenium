import requests
from bs4 import BeautifulSoup
import mechanicalsoup
import csv
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
import time
import pandas as pd


def Historia_gov(vin,plate,date):

    driver = webdriver.Chrome()
    # Adres URL strony do odpytania
    url = 'https://historiapojazdu.gov.pl/'
    # Przejdź do strony
    driver.get(url)

    # Znajdź pola formularza
    numer_rej_input = driver.find_element(By.XPATH,'//input[@id="_historiapojazduportlet_WAR_historiapojazduportlet_:rej"]')
    numer_vin_input = driver.find_element(By.XPATH,'//input[@id="_historiapojazduportlet_WAR_historiapojazduportlet_:vin"]')
    # wkleja dane na strone
    numer_rej_input.send_keys(plate)
    numer_vin_input.send_keys(vin)

    data_rej_input = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, '_historiapojazduportlet_WAR_historiapojazduportlet_:data'))
    )
    # Clear the field and fill
    driver.execute_script("arguments[0].value = arguments[1];", data_rej_input, date)

    # Znajdź przycisk "Sprawdź pojazd" i kliknij
    sprawdz_btn = driver.find_element(By.ID, '_historiapojazduportlet_WAR_historiapojazduportlet_:btnSprawdz')
    sprawdz_btn.click()


    marka_element = driver.find_element(By.ID, '_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt10:marka')
    model_element = driver.find_element(By.ID, '_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt10:model')
    vin_element = driver.find_element(By.CSS_SELECTOR, "p.vin span.strong")
    oc_element = driver.find_element(By.CSS_SELECTOR, "p.oc span.strong")
    tech_element = driver.find_element(By.CSS_SELECTOR, "p.tech span.strong")
    km_element = driver.find_element(By.CSS_SELECTOR, "p.km span.strong")
    status_element = driver.find_element(By.CSS_SELECTOR, "p.status span.strong")

    group_text_div = driver.find_element(By.CLASS_NAME, 'group-text')

    try:
        paliwo_span = group_text_div.find_element(By.XPATH, "//span[text()='Paliwo alternatywne: ']")
        paliwo_alternatywne = paliwo_span.find_element(By.XPATH, "./following-sibling::strong/span[@class='value']").text
    except NoSuchElementException:
        paliwo_alternatywne = "brak"

    try:
        masa_span = group_text_div.find_element(By.XPATH, "//span[text()='Masa własna pojazdu: ']")
        masa = masa_span.find_element(By.XPATH, "./following-sibling::strong/span[@class='value']").text
    except NoSuchElementException:
        masa = "brak"

    # Znajdź przycisk "Oś czasu" i kliknij
    kliknij_os_czasu= driver.find_element(By.ID, "raport-content-template-timeline-button")
    kliknij_os_czasu.click()

    group_box_div = driver.find_element(By.CLASS_NAME, 'group-box')

    liczba_wlascicieli_span = group_box_div.find_element(By.XPATH, "//p[contains(text(),'Liczba właścicieli: ')]/span[@class='value']")
    liczba_wspolwlascicieli_span = group_box_div.find_element(By.XPATH, "//p[contains(text(),'Liczba współwłaścicieli: ')]/span[@class='value']")
    liczba_wlascicieli_od_poczatku_span = group_box_div.find_element(By.XPATH, "//p[contains(text(),'Właściciele (od rejestracji do wygenerowania raportu): ')]/span[@class='value']")

    marka = marka_element.text
    model = model_element.text
    vin = vin_element.text
    oc = oc_element.text
    tech = tech_element.text
    km = km_element.text
    status = status_element.text
    liczba_wlascicieli = liczba_wlascicieli_span.text
    liczba_wspolwlascicieli = liczba_wspolwlascicieli_span.text
    liczba_wlascicieli_od_poczatku = liczba_wlascicieli_od_poczatku_span.text

    # Pobranie informacji z elementu <tbody> "oś czasu inwidualna dla każdego opdytania"
    tbody_element = driver.find_element(By.CSS_SELECTOR, "tbody")
    events = tbody_element.find_elements(By.CSS_SELECTOR, "tr.event")

    # Inicjalizacja pustych list dla kolumn
    dates_list = []
    descriptions_list = []
    odczytane_stany_list = []

    for event in events:
        date = event.find_element(By.CSS_SELECTOR, "td.date p").text
        description_element = event.find_element(By.CSS_SELECTOR, "td.description p")
        description = description_element.text
        elements = event.find_elements(By.CLASS_NAME,"licznik_wartosc.value")
        odczytany_stan=elements[0].text if elements else "Brak informacji"

        # Dodanie danych do odpowiednich list
        dates_list.append(date)
        descriptions_list.append(description)
        odczytane_stany_list.append(odczytany_stan)


    # Zamknij przeglądarkę
    driver.quit()

    df1 = pd.DataFrame({
        'Vin': [vin] * len(dates_list),
        'Data': dates_list,
        'Opis': descriptions_list,
        'Odczytany_stan_drogomierza': odczytane_stany_list
    })

    df2 = pd.DataFrame({
        'Marka': [marka],
        'Model': [model],
        'Vin': [vin],
        'Polisa_OC': [oc],
        'Badanie_techniczne': [tech],
        'Ostatni_stan_drogomierza': [km],
        'Status_rejestracji': [status],
        'Masa_wlasna_pojazdu': [masa],
        'Paliwo_alternatywne': [paliwo_alternatywne],
        'Liczba_wlascicieli': [liczba_wlascicieli],
        'Liczba_wspolwlascicieli': [liczba_wspolwlascicieli],
        'Liczba_wlascicieli_od_poczatku': [liczba_wlascicieli_od_poczatku]

    })

    return df1,df2

# Wczytaj plik CSV do obiektu DataFrame
file_path = 'csv_file.csv'
df = pd.read_csv(file_path, sep=';').astype(str)


historia_pojazdu_os_czasu = pd.DataFrame()
historia_pojazdu_base = pd.DataFrame()

for index, row in df.iterrows():
    vin = row['vin']
    plate = row['plate']
    date = row['Data_1_Rej']
    try:
        historia_pojazdu_os_czasu = pd.concat([historia_pojazdu_os_czasu, df1], ignore_index=True)
        historia_pojazdu_base = pd.concat([historia_pojazdu_base, df2], ignore_index=True)
    except NoSuchElementException:
        continue

historia_pojazdu_os_czasu.to_csv('historia_pojazdu_os_czasu.csv', index=False)
historia_pojazdu_base.to_csv('historia_pojazdu_base.csv', index=False)